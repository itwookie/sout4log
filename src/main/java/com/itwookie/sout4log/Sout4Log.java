package com.itwookie.sout4log;

import java.io.IOException;
import java.io.PrintStream;

/**
 * Main class for Sout4Log. This class handles standard stream injections and Logger management.
 * If you do not intend to use Loggers, you MUST call Sout4Log.install() to trigger the static initialization block
 * to be loaded by the VM. Otherwise using any member method like #getLogger() will init as well.<br>
 * Once Sout4Log has hooked standard stream they can not be unhooked again.<br>
 * <br>
 * <pre>{@code
 *     Sout4Log.install(); //For good practice
 *     //Minimal example registering a second log target besides stdout:
 *     Sout4Log.getDefaultLogger().addTarget(Level.ALL, s->filePrintStream.println(s));
 *     //If you don't want to log to actual system out, but still use System.out (maybe due to legacy systems)
 *     // you can also remove the system streams from the default logger. Be aware: At least one output target has to
 *     // be registered or the framework will throw a NoLoggingTargetsException
 *     Sout4Log.getDefaultLogger().removeTarget(Level.ALL, Sout4Log.getSOutTarget());
 *     //You can also change the log level for sout and serr, default is INFO
 *     Sout4Log.setSOutSeverity(Level.Debug);
 *     //Finally you can use sout for logging to the default logger
 *     System.out.println("My debug message");
 *     //... or use a Logger instance as you're used to
 *     Sout4Log.getLogger("default").info("This is an info message");
 * }</pre>
 */
public class Sout4Log {

    static { init(); } /** bypass static init code size limit */
    private static void init() {
        //hook original sout and serr
        actualSOut = System.out;
        actualSErr = System.err;
        //set up default logger
        DEFAULT_LOGGER = getLogger("default");
        DEFAULT_LOGGER.addTarget(Level.ALL, getSOutTarget()); //log <WARNING to sout
        DEFAULT_LOGGER.addTarget(Level.WARNING, getSErrTarget()); // log >=Warning to serr
        //redirect sout and serr to the defulat logger
        soutLogStream = new LoggingStream(Level.INFO);
        serrLogStream = new LoggingStream(Level.WARNING);
        System.setOut(new PrintStream(soutLogStream));
        System.setErr(new PrintStream(serrLogStream));
        //properly close all loggers on shutdown
        Runtime.getRuntime().addShutdownHook(new ShutdownHook());
    }

    private static Logger DEFAULT_LOGGER;
    private static LoggingStream soutLogStream;
    private static LoggingStream serrLogStream;
    private static PrintStream actualSOut;
    private static PrintStream actualSErr;
    public static LogTarget getSOutTarget() {
        return LogTarget.fromPrintStream(actualSOut);
    }
    public static LogTarget getSErrTarget() {
        return LogTarget.fromPrintStream(actualSErr);
    }
    static PrintStream getSErrForEmergencies() { return actualSErr; }

    public static Logger getLogger(String name) {
        return Logger.getOrCreate(name);
    }
    /**
     * Returns the defulat logger. This logger is attached to and wrapping the system streams System.out and System.err.
     * So whenever you write to one of the default streams, it will pass though the default loggers formatting first,
     * before being passed to the actual system streams. You can specify the default streams severity using
     * #setSOutSeverity() and #setSErrSeverity() to influence the log targets for both streams individually, even
     * merging them into system defaults out.
     *
     * @return the logger named "default
     */
    public static Logger getDefaultLogger() {
        return DEFAULT_LOGGER;
    }
    /**
     * Set the severity for messages logged using System.out
     * @param level the new severity value
     * @throws IOException if the buffer for the previous severity couldn't be flushed
     */
    public static void setSOutSeverity(Level level) throws IOException {
        soutLogStream.setEmitLevel(level);
    }
    /**
     * Set the severity for messages logged using System.err
     * @param level the new severity value
     * @throws IOException if the buffer for the previous severity couldn't be flushed
     */
    public static void setSErrSeverity(Level level) throws IOException {
        serrLogStream.setEmitLevel(level);
    }

    /**
     * All initialisation is static, so if you use any other Sout4Log method before logging, you probably don't
     * need to explicitly call this. If however you plan to only use System.out, you MUST call this function to load
     * Sout4Log in the VM so static init is run.<br>
     * Once sout4log is hooking the standard streams you can not unhook it again.
     */
    public static void install() {
        /* dummy for static init */
        if (actualSOut == null) init();
    }

    /**
     * This is a utility to switch the active logger for sout. With the LoggingContext, please log though the context
     * or sout, as using Logger.log tries to temporarily get a context as well, triggering an LoggerAlreadyActiveException.<br>
     * Will wait until the previous loggers have written their messages and were released.
     *
     * @param logger the Logger to push on the stack
     * @return autoCloseable context for the Logger
     */
    public static synchronized LoggingContext withLogger(Logger logger) {
        return new LoggingContext(logger);
    }

    static void flushStandardStreams() {
        try { soutLogStream.flush(); } catch (IOException ignore) {}
        try { serrLogStream.flush(); } catch (IOException ignore) {}
    }

}
