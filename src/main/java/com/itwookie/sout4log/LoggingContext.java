package com.itwookie.sout4log;

import com.itwookie.sout4log.exceptions.ContextStackDesynchronizedException;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * With this logging framework, it is intended to also log with System.out, but you
 * might still want to use different loggers or formats for different situations like different threads or systems.
 * For those cases you can push loggers as active logger onto the context stack with something like
 * <pre>{@code
 * try (LoggingContext subContext = Sout4Log.withLogger(customLogger)) {
 *     System.out.println("This is now logging to a different logger");
 * }
 * System.out.println("This will use the default logger again");
 * }</pre>
 * If you want to log at the level of a specific logging context, event if it is not at the top, of if you don not
 * have a logger instance, you can also call log() on the LoggingContext-Object as a wrapper to the Logger.
 */
public class LoggingContext implements AutoCloseable, Logging {

    static final Map<Thread, Stack<Logger>> contextStacks = new HashMap<>();
    private static void push(Logger logger) {
        contextStacks.computeIfAbsent(Thread.currentThread(), x->new Stack<>()).push(logger);
    }
    static Logger getActiveLogger() {
        Stack<Logger> lstack = contextStacks.get(Thread.currentThread());
        return (lstack != null && !lstack.empty()) ? lstack.peek() : Sout4Log.getDefaultLogger();
    }
    private static void pop(Logger logger) {
        Stack<Logger> lstack = contextStacks.get(Thread.currentThread());
        if (lstack == null || lstack.empty() || lstack.peek() != logger)
            throw new ContextStackDesynchronizedException("Logger '"+logger.getName()+"' was not on top of the current threads logger context stack");
        lstack.pop();
        if (lstack.empty()) contextStacks.remove(Thread.currentThread());
    }

    private final Logger logger;
    LoggingContext(Logger logger) {
        push(this.logger = logger);
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public void close() {
        pop(logger);
    }

    @Override
    public void log(Level level, String message) {
        logger.log(level, message);
    }

    @Override
    public void trace(String message) {
        logger.log(Level.ERROR, message);
    }

    @Override
    public void debug(String message) {
        logger.log(Level.ERROR, message);
    }

    @Override
    public void info(String message) {
        logger.log(Level.ERROR, message);
    }

    @Override
    public void warn(String message) {
        logger.log(Level.ERROR, message);
    }

    @Override
    public void error(String message) {
        logger.log(Level.ERROR, message);
    }
}
