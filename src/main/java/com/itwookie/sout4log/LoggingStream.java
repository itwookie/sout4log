package com.itwookie.sout4log;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * This is a utility class for wrapping sout and serr and redirecting them to the currently active logger context.
 * This object will auto flush with every line end written, windows and linux line ends supported.
 * Flushing this stream manually will trigger output if not empty, forcing a line end,
 * regardless of whether the buffer contained one.
 */
public class LoggingStream extends OutputStream {

    private final StringBuffer buffer = new StringBuffer();
    private Level level;

    LoggingStream(Level level) {
        super();
        this.level = level;
    }

    /** This will flush the buffer and change the severity for further output.
     * This means that after this call, byte written to this stream might be sent to different
     * LogTargets. */
    void setEmitLevel(Level level) throws IOException {
        if (level == null) throw new NullPointerException("Logging level can't be null!");
        this.flush();
        this.level = level;
    }

    @Override
    public void write(byte[] b) throws IOException {
        buffer.append(new String(b, StandardCharsets.UTF_8));
        autoFlush(false);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        buffer.append(new String(b, off, len));
        autoFlush(false);
    }

    private int nextLineEnd() {
        for (int i=0; i<buffer.length(); i++) {
            char c = buffer.charAt(i);
            if (c=='\r'||c=='\n') return i;
        }
        return -1;
    }
    private int lineEndWidth(int index) {
        char c = buffer.charAt(index);
        if (c=='\n') return 1; //unix \n
        else if (c=='\r') {
            //check for windows line end \r\n
            if (index >= buffer.length()) return 1;
            else if (buffer.charAt(index+1) == '\n') return 2;
            else return 1; //osx \r
        } else return 0; //what are you doing here?
    }
    void autoFlush(boolean empty) throws IOException {
        int at;
        Logging logger = LoggingContext.getActiveLogger();
        while ((at=nextLineEnd())>=0) {
            String substring = buffer.substring(0, at); //skip newline here, .log() adds that back
            buffer.delete(0, at+lineEndWidth(at)); //pop newline as well
            logger.log(level, substring); //produce outputs
        }
        if (empty && buffer.length()>0) {
            logger.log(level, buffer.toString());
            buffer.setLength(0);
        }
    }

    /**
     * Force the current buffer to be sent to the active logger, if the buffers are not empty.
     * Line ends automatically flush, but in case no line end was appended yet, it will treat the buffer as if it ended
     * with one.
     */
    @Override
    public void flush() throws IOException {
        autoFlush(true);
    }

    @Override
    public void close() throws IOException {
        //can't close standard streams
    }

    @Override
    public void write(int b) throws IOException {
        buffer.append((char)(b&255));
        autoFlush(false);
    }
}
