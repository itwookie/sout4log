package com.itwookie.sout4log;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public interface LogTarget {

    void log(String message) throws IOException;

    /** in case your log target requires closing, you can implement this class. This should be no-throw! */
    default void close() {};

    static LogTarget fromPrintStream(PrintStream ps) {
        return new SynchronizedTarget<>(ps);
    }
    static LogTarget fromOutputStream(OutputStream out) {
        return new SynchronizedTarget<>(out);
    }

}
