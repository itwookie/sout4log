package com.itwookie.sout4log;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface LogFormatter {

    String log(Level level, Logger logger, String message);

    static LogFormatter createDefault() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return (lvl,l,msg)->String.format("[%s] [%s @ %s] %s: %s", sdf.format(new Date()), l.getName(), Thread.currentThread().getName(), lvl.name(), msg);
    }

}
