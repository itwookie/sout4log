package com.itwookie.sout4log;

public enum Level {

    ALL(Integer.MIN_VALUE),
    TRACE(400),
    DEBUG(500),
    INFO(800),
    WARNING(900),
    ERROR(1000),
    OFF(Integer.MAX_VALUE),
    ;

    public final int severity;
    private Level(int severity) { this.severity = severity; }

}
