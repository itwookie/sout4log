package com.itwookie.sout4log;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * This is a wrapper for LogTargets that synchronizes OutputStream access to prevent
 * message mix-ups across different threads.
 */
public class SynchronizedTarget<T extends OutputStream> implements LogTarget {

    T out;
    public SynchronizedTarget(T out) {
        this.out = out;
    }

    @Override
    public synchronized void log(String message) throws IOException {
        out.write(message.getBytes(StandardCharsets.UTF_8));
        out.write(System.lineSeparator().getBytes(StandardCharsets.UTF_8));
        out.flush();
    }

    @Override
    public synchronized void close() {
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace(Sout4Log.getSErrForEmergencies());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SynchronizedTarget)) return false;
        SynchronizedTarget<?> that = (SynchronizedTarget<?>) o;
        return Objects.equals(out, that.out);
    }

    @Override
    public int hashCode() {
        return Objects.hash(out);
    }
}
