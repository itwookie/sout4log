package com.itwookie.sout4log;

import java.io.IOException;

public interface Logging {

    void log(Level level, String message);
    void trace(String message);
    void debug(String message);
    void info(String message);
    void warn(String message);
    void error(String message);

}
