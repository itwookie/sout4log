package com.itwookie.sout4log.exceptions;

public class ContextStackDesynchronizedException extends RuntimeException {

    public ContextStackDesynchronizedException() {
    }

    public ContextStackDesynchronizedException(String message) {
        super(message);
    }

    public ContextStackDesynchronizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContextStackDesynchronizedException(Throwable cause) {
        super(cause);
    }
}
