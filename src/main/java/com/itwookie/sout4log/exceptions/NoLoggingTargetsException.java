package com.itwookie.sout4log.exceptions;

public class NoLoggingTargetsException extends RuntimeException {

    public NoLoggingTargetsException() {
    }

    public NoLoggingTargetsException(String message) {
        super(message);
    }

    public NoLoggingTargetsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoLoggingTargetsException(Throwable cause) {
        super(cause);
    }
}
