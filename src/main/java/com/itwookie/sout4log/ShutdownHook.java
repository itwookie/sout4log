package com.itwookie.sout4log;

class ShutdownHook extends Thread {

    @Override
    public void run() {
        Sout4Log.flushStandardStreams();
        LoggingContext.contextStacks.forEach((key, value) -> {
            //notify the threads, raises interrupted exceptions
            try { key.notifyAll(); }
            catch (IllegalMonitorStateException ignore) { }
            //close all loggers, this should close all targets that need closing
            value.forEach(Logger::close);
        });
    }
}
