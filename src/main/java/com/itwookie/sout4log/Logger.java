package com.itwookie.sout4log;

import com.itwookie.sout4log.exceptions.NoLoggingTargetsException;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

/**
 *
 */
public class Logger implements Logging, Closeable {

    private static final Map<String, Logger> namedLogger = new HashMap<>();
    /**
     * Get or create a chached logger instance, so you can get the logger by name over and over,
     * without losing registered targets and formatters. This is wrapped by Sout4Log.getLogger()
     *
     * @param name the name of this logger
     * @return New or existing logger with the specified name
     */
    public static Logger getOrCreate(String name) {
        return namedLogger.computeIfAbsent(name, Logger::new);
    }

    private boolean wasClosed=false;
    private final Map<Integer, LogFormatter> formatting;
    private final String name;
    private final SortedMap<Integer, Set<LogTarget>> targets;
    protected Logger(String name) {
        if (namedLogger.containsKey(name))
            throw new IllegalArgumentException("Logger with this name already exits");
        this.targets = new TreeMap<>(Integer::compareTo);
        this.formatting = new HashMap<>();
        this.name = name;
        setFormat(Level.ALL, LogFormatter.createDefault());
        addTarget(Level.ALL, Sout4Log.getSOutTarget());
        addTarget(Level.WARNING, Sout4Log.getSErrTarget());
    }
    protected LogFormatter getFormat(Level level) {
        LogFormatter fmt = formatting.get(level.severity);
        if (fmt == null) fmt = formatting.get(Level.ALL.severity);
        return fmt;
    }
    public void setFormat(Level level, LogFormatter formatter) {
        formatting.put(level.severity, formatter);
    }
    /**
     * Add a logging target for any message with at least this severity.
     * If there are any targets with a higher minLevel that's &lt;= the message severity, it will take precedence. <br>
     * E.g. add a Target for Level.All and one for Level.Warning means that all messages with a severity
     * [Level.All .. Level.Warning) are logged to the target added for Level.All. Messages with a severity &gt;=
     * Level.Warning will go to the targets for Level.Warning. <br>
     * Every Level that defines ANY target will overshadow ALL targets of lower Levels!
     *
     * @param minLevel the minimum severity to register this target for
     * @param target   the wrapped output stream to log to
     * @return true if this target was added at the specified level
     */
    public boolean addTarget(Level minLevel, LogTarget target) {
        return targets.computeIfAbsent(minLevel.severity, s->new HashSet<>()).add(target);
    }
    /**
     * Removes the target for messages of this severity and above.
     *
     * @param minLevel the minimum severity that target was registered to
     * @param target the target to remove
     * @see #addTarget(Level, LogTarget)
     * @return true if the target was previously registered at the specified level
     */
    public boolean removeTarget(Level minLevel, LogTarget target) {
        if (!targets.containsKey(minLevel.severity)) return false;
        Set<LogTarget> set = targets.get(minLevel.severity);
        boolean removed = set.remove(target);
        if (set.isEmpty()) targets.remove(minLevel.severity);
        return removed;
    }
    /**
     * Find the set of target with the highest severity &lt;= the requested level.
     *
     * @param logLevel the maximum severity to look for loggers
     * @return the set of targets responsible for the specified severity
     */
    public Set<LogTarget> getTargets(Level logLevel) {
        int maxFound = Integer.MIN_VALUE;
        boolean anyFound = false;
        for (int severity : targets.keySet()) {
            if (severity <= logLevel.severity) {
                anyFound = true;
                //since keySet is ascending, this will automatically return max if we let the loop go on
                maxFound = severity;
            }
        }
        if (!anyFound) throw new NoLoggingTargetsException("The logger '"+name+"' has no registered targets!");
        return targets.get(maxFound);
    }

    public String getName() {
        return name;
    }

    /**
     * Internal logging function, formatting the message and forwarding it to all targets.
     * This should not be called manually, use Logger.log() or LoggingTicket.log() instead!
     *
     * @param level the severity of this log entry
     * @param msg the log entry itself
     */
    @Override
    public void log(Level level, String msg) {
        if (level.severity == Level.OFF.severity) return;
        String formatted = getFormat(level).log(level, this, msg);
        for (LogTarget target : getTargets(level))
            try {
                target.log(formatted);
            } catch (IOException e) {
                e.printStackTrace(Sout4Log.getSErrForEmergencies());
            }
    }

    @Override
    public void trace(String message) {
        log(Level.TRACE, message);
    }

    @Override
    public void debug(String message) {
        log(Level.DEBUG, message);
    }

    @Override
    public void info(String message) {
        log(Level.INFO, message);
    }

    @Override
    public void warn(String message) {
        log(Level.WARNING, message);
    }

    @Override
    public void error(String message) {
        log(Level.ERROR, message);
    }

    /**
     * In case you're not holding references to your LogTargets and need to close all streams open by LogTargets,
     * you can call close() to dispose all open handles
     */
    @Override
    public void close() {
        if (wasClosed) return;
        wasClosed = true;
        Set<LogTarget> allTargets = new HashSet<>();
        for (Set<LogTarget> targetSet : targets.values()) allTargets.addAll(targetSet);
        for (LogTarget target : allTargets) try {
            target.close();
        } catch (Throwable ignore) {}
    }
}
