package com.itwookie.sout4log.test;

import com.itwookie.sout4log.Logger;
import com.itwookie.sout4log.Sout4Log;
import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

public class SoutLoggingTest extends CommonTestRedirects {

    @Test
    public void DefaultLoggerTargetSOut() {
        Logger logger = Sout4Log.getDefaultLogger();
        logger.info("Info message");
        System.out.flush(); //should flush to bosOut
        sout.println("\""+bosOut.toString()+"\"");
        Pattern pattern = Pattern.compile("^\\[[0-9A-Z:.+-]+] \\[[\\w ]+ @ [\\w ]+] INFO: Info message\\r?\\n$", Pattern.MULTILINE);
        assert pattern.matcher(bosOut.toString()).matches(); //default format
    }

    @Test
    public void SOutLoggingTargetDefaultLogger() {
        Sout4Log.install();
        System.out.println("Test Sout message");
        sout.println("\""+bosOut.toString()+"\"");
        Pattern pattern = Pattern.compile("^\\[[0-9A-Z:.+-]+] \\[[\\w ]+ @ [\\w ]+] INFO: Test Sout message\\r?\\n$", Pattern.MULTILINE);
        assert pattern.matcher(bosOut.toString()).matches(); //default format
    }

}
