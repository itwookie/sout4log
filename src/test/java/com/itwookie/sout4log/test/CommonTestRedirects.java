package com.itwookie.sout4log.test;

import com.itwookie.sout4log.Sout4Log;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;

public abstract class CommonTestRedirects {

    static PrintStream sout;
    static PrintStream serr;

    static ByteArrayOutputStream bosOut;
    static ByteArrayOutputStream bosErr;

    @BeforeAll
    public static void RedirectStreams() {
        sout = System.out;
        serr = System.err;
        bosOut = new ByteArrayOutputStream();
        bosErr = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bosOut));
        System.setErr(new PrintStream(bosErr));
        Sout4Log.install();
    }

    @BeforeEach
    public void ClearBuffers() {
        bosOut.reset();
        bosErr.reset();
    }

    @AfterEach
    public void EchoErrorStream() {
        String error = bosErr.toString();
        if (!error.isEmpty()) System.err.println(error);
    }

    @AfterAll
    public static void RestoreStreams() {
        System.setOut(sout);
        System.setErr(serr);
        //reset the member tested to allow re-hooking streams by other tests
        try {
            Field f = Sout4Log.class.getDeclaredField("actualSOut");
            f.setAccessible(true);
            f.set(null, null);
            f.setAccessible(false);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

}
