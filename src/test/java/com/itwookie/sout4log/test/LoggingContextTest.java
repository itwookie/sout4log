package com.itwookie.sout4log.test;

import com.itwookie.sout4log.Level;
import com.itwookie.sout4log.Logger;
import com.itwookie.sout4log.LoggingContext;
import com.itwookie.sout4log.Sout4Log;
import org.junit.jupiter.api.Test;

public class LoggingContextTest extends CommonTestRedirects {

    @Test
    public void BasicContextTest() {
        Logger stack1 = Sout4Log.getLogger("stack1");
        Logger stack2 = Sout4Log.getLogger("stack2");
        try (LoggingContext ctx1 = Sout4Log.withLogger(stack1)) {
            try (LoggingContext ctx2 = Sout4Log.withLogger(stack2)) {
                System.out.println("Alpha");
                ctx1.log(Level.INFO, "Beta");
            }
            System.out.println("Gamma");
        }
        System.out.println("Delta");
        //validate
        String[] lines = bosOut.toString().trim().split("\r?\n");
        assert lines.length == 4;
        //check that the messages are printed with the correct context(=logger)
        assert lines[0].contains("Alpha") && lines[0].contains("stack2");
        assert lines[1].contains("Beta") && lines[1].contains("stack1");
        assert lines[2].contains("Gamma") && lines[2].contains("stack1");
        assert lines[3].contains("Delta") && lines[3].contains("default");
    }

}
