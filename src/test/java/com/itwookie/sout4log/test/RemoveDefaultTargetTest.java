package com.itwookie.sout4log.test;

import com.itwookie.sout4log.Level;
import com.itwookie.sout4log.Logger;
import com.itwookie.sout4log.Sout4Log;
import com.itwookie.sout4log.SynchronizedTarget;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.util.regex.Pattern;

public class RemoveDefaultTargetTest extends CommonTestRedirects {

    @Test
    public void DefaultLogToCustomTarget() {
        //Setup
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        SynchronizedTarget<ByteArrayOutputStream> target = new SynchronizedTarget<>(baos);
        Logger defaultLogger = Sout4Log.getDefaultLogger();
        assert defaultLogger.removeTarget(Level.ALL, Sout4Log.getSOutTarget());
        assert defaultLogger.addTarget(Level.ALL, target);
        //Logging
        System.out.println("Test message");
        //validation
        sout.println("StdOut \""+bosOut.toString()+"\"");
        sout.println("Custom \""+baos.toString()+"\"");
        Pattern pattern = Pattern.compile("^\\[[0-9A-Z:.+-]+] \\[[\\w ]+ @ [\\w ]+] INFO: Test message\\r?\\n$", Pattern.MULTILINE);
        assert bosOut.toString().isEmpty();
        assert pattern.matcher(baos.toString()).matches();
    }

}
