# Sout4Log

Did your boss suddenly tell you to use a Logger despite hearing of Log4J? Did you suddenly realize that println("here")
kind of doesn't work for multithreaded applications? Just want to add file logging to your mess that is standard out
messages? Don't want to add a proper logging framework because find & replace is hard, or because you're coding your
most recent project using word and changing stuff would be a pain? *Someone said System.out for logging is supposed to
be bad?* <font size="0.5">(What a party pooper)</font>

Then this project is for you! Sout4Log provides a multi-target logging framework with log-level dependent log entry
formatting, that should be thread save (if you implement your `LogTarget`s threadsafe)! And the best of all? You don't
need to use `Logger.info()` or `Logger.log()` because this framework automatically wraps the standard output stream.
Just keep using `System.out.println()` (or don't, I'm none of your parents).

## Usage

If you do not intend to change anything and just use the default Logger, all you need to do is add the following line
before the first `println()`:
```java
public static void main(String[] args) {
    Sout4Log.install();
    //other stuff
    System.out.println("This now goes to the default logger");
    Sout4Log.getDefaultLogger().info("This is equivalent to System.out.println");
}
```
But this is by far not all! If you want a custom logging format for a certain logging level, you can do so with
`logger.setFormat(level, (level,logger,message)->formattedMessage)`.
Please note that currently the level is exact, with the formatter for Level.ALL as fallback.

The default format is `[ISO TIME] [LOGGER @ THREAD] SEVERITY: MESSAGE`

You can also create other Loggers, remove the default targets for `Sout4Log.getSOutTarget()` and
`Sout4Log.getSErrTarget()` and add custom file targets. If you need thread safety, there is a minimal implementation for
`SynchronizedTarget`, that will wrap an output stream and synchronize log() calls.

Registering logging targets is done to logging level ranges. Each set of targets added to one severity is active up to
the next severity level that registers at least one logging target. This means if you add a logging target to Level.ALL
and one target to Level.WARN, the target added to Level.ALL will be used for all messages where
Level.ALL.severity >= severity < Level.WARN.severity.

Lastly you can use different Loggers in different contexts, like threads, subsystems, ect. This is done by creating a
`LoggingContext`. Logging contexts are `AutoCloseable` so you can use them with try-resource blocks.
```java
try (LoggingContext sublogger = Sout4Log.getLogger("subsystem")) {
    Syste.out.println("This now goes to a different logger");
    sublogger.info("You can also log using the context or directly through the logger of course");
}
```
About logging target resources: Since logging targets may be files or other stream based resources that might have to be
flushed and closed, the targets are flushed after every message.
Additionally there's a shutdown hook added that will automatically close all open loggers on nominal terminations of the
JVM, subsequently calling `.close()` on all logging targets.

## JitPack

To get a Git project into your build:

* Step 1. Add the JitPack repository to your build file  
Add it in your root build.gradle at the end of repositories:
```groovy
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```
* Step 2. Add the dependency
```groovy
dependencies {
    implementation 'com.gitlab.itwookie:sout4log:master-SNAPSHOT'
}
```

## About

Ok, so this project was not meant to be super serious, but it might still hold value for when you need a quick, simple
logging framework, that does no more than what it says on it's tin. I didn't really Like how java's logging levels are
a subclass, so I basically wrote a duplicate enum.
I would have liked to use the existing logging classes, but I couldn't find a factory, builder, registry, manager,
&lt;insert different concept here> for those, so I wrote a minimal logger class myself.

If nothing else, this project is aimed to show, that it's not hard to redirect standard streams to implement logging to
different targets at the same time.

## Lisence

This project is MIT Licensed, check LICENSE.md for the full copy&paste.

## Dependencies

None, this is pure java, and should even still run on Java8.
